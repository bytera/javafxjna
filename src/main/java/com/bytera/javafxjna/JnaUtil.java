package com.bytera.javafxjna;

import java.util.Arrays;
import java.util.List;

import javafx.geometry.Insets;

import com.sun.glass.ui.Window;
import com.sun.jna.Function;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.ptr.IntByReference;

import lombok.extern.slf4j.Slf4j;

import static com.sun.jna.platform.win32.WinUser.*;

@Slf4j
public final class JnaUtil {

    private static final User32 USER_32 = User32.INSTANCE;
    private static final int BUFFER_512 = 512;

    /*
     * Window Styles
     */
    public static final int WS_OVERLAPPED = 0x00000000;
    public static final int WS_POPUP = 0x80000000;
    public static final int WS_CHILD = 0x40000000;
    public static final int WS_MINIMIZE = 0x20000000;
    public static final int WS_VISIBLE = 0x10000000;
    public static final int WS_DISABLED = 0x08000000;
    public static final int WS_CLIPSIBLINGS = 0x04000000;
    public static final int WS_CLIPCHILDREN = 0x02000000;
    public static final int WS_MAXIMIZE = 0x01000000;
    public static final int WS_CAPTION = 0x00C00000;     /* WS_BORDER | WS_DLGFRAME  */
    public static final int WS_BORDER = 0x00800000;
    public static final int WS_DLGFRAME = 0x00400000;
    public static final int WS_VSCROLL = 0x00200000;
    public static final int WS_HSCROLL = 0x00100000;
    public static final int WS_SYSMENU = 0x00080000;
    public static final int WS_THICKFRAME = 0x00040000;
    public static final int WS_GROUP = 0x00020000;
    public static final int WS_TABSTOP = 0x00010000;

    public static final int WS_MINIMIZEBOX = 0x00020000;
    public static final int WS_MAXIMIZEBOX = 0x00010000;

    //Desktop Window Manager
    private static NativeLibrary DWM = null;

    private JnaUtil() {
    }

    /**
     * Get native window handler
     *
     * @param mainWindowTitle
     * @return
     */
    public static HWND getWinHwnd(final String mainWindowTitle) {
        List<Window> windows = Window.getWindows();

        for (Window window : windows) {
            long lhwnd = window.getNativeWindow();
            Pointer lpVoid = new Pointer(lhwnd);
            WinDef.HWND hwnd = new WinDef.HWND(lpVoid);

            // only add to main window
            char[] windowText = new char[BUFFER_512];
            USER_32.GetWindowText(hwnd, windowText, BUFFER_512);
            String text = Native.toString(windowText);
            if (!text.isEmpty() && text.equals(mainWindowTitle)) {
                return hwnd;
            }
        }
        return null;
    }

    public static void removeWindowStyle(final HWND hWnd, final int styleModification) {
        int oldStyle = USER_32.GetWindowLong(hWnd, GWL_STYLE);
        int newStyle = oldStyle & ~styleModification;
        USER_32.SetWindowLong(hWnd, GWL_STYLE, newStyle);

    }

    public static void addWindowStyle(final HWND hWnd, int styleModification) {
        int oldStyle = USER_32.GetWindowLong(hWnd, GWL_STYLE);
        int newStyle = oldStyle | styleModification;
        USER_32.SetWindowLong(hWnd, GWL_STYLE, newStyle);
    }

    public static void addWindowExtStyle(final HWND hWnd, int styleModification) {
        int oldStyle = USER_32.GetWindowLong(hWnd, GWL_EXSTYLE);
        int newStyle = oldStyle | styleModification;
        USER_32.SetWindowLong(hWnd, GWL_EXSTYLE, newStyle);
    }

    public static boolean isWinStyleEnabled() {
        if (Platform.isWindows() && DWM == null) {
            try {
                DWM = NativeLibrary.getInstance("dwmapi");
            } catch (Throwable e) {
                log.error("", e);
            }
        }

        boolean dwmEnabled = false;

        if (DWM != null) {
            boolean[] bool = {false};
            Object[] args = {bool};
            Function DwmIsCompositionEnabled = DWM.getFunction("DwmIsCompositionEnabled");
            WinNT.HRESULT result = (WinNT.HRESULT) DwmIsCompositionEnabled.invoke(WinNT.HRESULT.class, args);
            boolean success = result.intValue() == 0;

            if (success && bool[0]) {
                dwmEnabled = true;
            }
        }

        return dwmEnabled;
    }

    public static void setMargin(final HWND window, final Insets insets) {
        if (isWinStyleEnabled()) {
            MARGINS margins = new MARGINS();
            margins.cyTopHeight = insets == null ? 0 : (int) insets.getTop();
            margins.cxRightWidth = insets == null ? 0 : (int) insets.getRight();
            margins.cyBottomHeight = insets == null ? 0 : (int) insets.getBottom();
            margins.cxLeftWidth = insets == null ? 0 : (int) insets.getLeft();

            //DwmExtendFrameIntoClientArea(HWND hWnd, MARGINS *pMarInset)
            Function extendFrameIntoClientArea = DWM.getFunction("DwmExtendFrameIntoClientArea");
            WinNT.HRESULT result = (WinNT.HRESULT) extendFrameIntoClientArea.invoke(WinNT.HRESULT.class, new Object[]{window, margins});
            if (result.intValue() != 0) {
                log.error("Call to DwmExtendFrameIntoClientArea failed.");
            }
        }
    }

    public static void setDwmRenderingPolicy(final HWND window) {
        int DWMNCRP_ENABLED = 2; // The non-client area rendering is enabled; the window style is ignored.
        Function extendFrameIntoClientArea = DWM.getFunction("DwmSetWindowAttribute");
        WinNT.HRESULT result = (WinNT.HRESULT) extendFrameIntoClientArea.invoke(WinNT.HRESULT.class, new Object[]{window, DWMNCRP_ENABLED, new IntByReference(DWMNCRP_ENABLED), 4});
        if (result.intValue() != 0) {
            log.error("Call to DwmSetWindowAttribute failed.");
        }
    }


    /**
     * http://msdn.microsoft.com/en-us/library/bb773244%28v=VS.85%29.aspx
     * <p>
     * Note: class and fields needs to be 'public' to be accessible for JNA calls.
     */
    public static class MARGINS extends Structure implements Structure.ByReference {
        public int cxLeftWidth;
        public int cxRightWidth;
        public int cyTopHeight;
        public int cyBottomHeight;

        @Override
        protected List getFieldOrder() {
            return Arrays.asList("cxLeftWidth", "cxRightWidth", "cyTopHeight", "cyBottomHeight");
        }
    }
}


