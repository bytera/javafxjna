package com.bytera.javafxjna;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import com.sun.jna.platform.win32.WinDef;

import lombok.extern.slf4j.Slf4j;

import static com.bytera.javafxjna.JnaUtil.*;

@Slf4j
public class AppStageJna extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.initStyle(StageStyle.TRANSPARENT);

        primaryStage.setTitle("Test");

        Label label = new Label("Test TRANSPARENT window.");
        BorderPane rootPane = new BorderPane(label);
        rootPane.setPrefSize(400, 300);
        rootPane.setTop(new Label("Test Top."));
        rootPane.setRight(new Label("Test Right."));
        rootPane.setBottom(new Label("Test bottom."));
        rootPane.setLeft(new Label("Test left."));
        //rootPane.setStyle("-fx-background-color: lightgray; -fx-background-radius: 1em; -fx-border-color: red; -fx-border-radius: 1em; -fx-padding: 1em;");
        Scene scene = new Scene(rootPane);
        scene.setFill(null);

        primaryStage.widthProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("widthProperty newValue = " + newValue);
        });
        primaryStage.setOnShown(event -> Platform.runLater(() -> setJnaStyle(primaryStage.getTitle())));
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    private void setJnaStyle(String title) {
        if (JnaUtil.isWinStyleEnabled()) {
            WinDef.HWND winHwnd = JnaUtil.getWinHwnd(title);

            if (winHwnd == null) {
                log.warn("Window handler is null !");
            }

            long style = WS_CAPTION | WS_OVERLAPPED;
            style |= WS_THICKFRAME; //resizable
            style |= WS_MINIMIZEBOX; //minimizable
            style |= WS_MAXIMIZEBOX; //maximizable

            JnaUtil.addWindowStyle(winHwnd, (int) style);
            JnaUtil.setMargin(winHwnd, new Insets(10));
        }
    }
}
